﻿using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Management.Automation.Runspaces;

using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using System.Security.Cryptography;

using System.IO;
using System.IO.Compression;
using System.Reflection;

using System.Collections.Generic;

using System.Runtime.InteropServices;

class Wrapper
{
	private static string OpenSSLDecrypt(byte[] encryptedBytesWithSalt, string passphrase, int blockSize, int keySize)
	{
		// extract salt (first 8 bytes of encrypted)
		byte[] salt = new byte[8];
		byte[] encryptedBytes = new byte[encryptedBytesWithSalt.Length - salt.Length - 8];
		Buffer.BlockCopy(encryptedBytesWithSalt, 8, salt, 0, salt.Length);
		Buffer.BlockCopy(encryptedBytesWithSalt, salt.Length + 8, encryptedBytes, 0, encryptedBytes.Length);
		// get key and iv
		byte[] key, iv;
		DeriveKeyAndIV(passphrase, blockSize, keySize, salt, out key, out iv);
		return DecryptStringFromBytesAes(encryptedBytes, blockSize, keySize, key, iv);
	}

	private static void DeriveKeyAndIV(string passphrase, int blockSize, int keySize, byte[] salt, out byte[] key, out byte[] iv)
	{
		blockSize /= 8;
		keySize /= 8;

		int requiredBytes = blockSize + keySize;
		// generate key and iv
		List<byte> concatenatedHashes = new List<byte>(requiredBytes);

		byte[] password = Encoding.UTF8.GetBytes(passphrase);
		byte[] currentHash = new byte[0];
		SHA1 sha1 = SHA1.Create();
		bool enoughBytesForKey = false;
		// See http://www.openssl.org/docs/crypto/EVP_BytesToKey.html#KEY_DERIVATION_ALGORITHM
		while (!enoughBytesForKey)
		{
			int preHashLength = currentHash.Length + password.Length + salt.Length;
			byte[] preHash = new byte[preHashLength];

			Buffer.BlockCopy(currentHash, 0, preHash, 0, currentHash.Length);
			Buffer.BlockCopy(password, 0, preHash, currentHash.Length, password.Length);
			Buffer.BlockCopy(salt, 0, preHash, currentHash.Length + password.Length, salt.Length);

			currentHash = sha1.ComputeHash(preHash);
			concatenatedHashes.AddRange(currentHash);

			if (concatenatedHashes.Count >= requiredBytes)
				enoughBytesForKey = true;
		}

		key = new byte[keySize];
		iv = new byte[blockSize];
		concatenatedHashes.CopyTo(0, key, 0, keySize);
		concatenatedHashes.CopyTo(keySize, iv, 0, blockSize);

		sha1.Clear();
		sha1 = null;
	}


	private static string DecryptStringFromBytesAes(byte[] cipherText, int blockSize, int keySize, byte[] key, byte[] iv)
	{
		// Check arguments.
		if (cipherText == null || cipherText.Length <= 0)
			throw new ArgumentNullException("cipherText");
		if (key == null || key.Length <= 0)
			throw new ArgumentNullException("key");
		if (iv == null || iv.Length <= 0)
			throw new ArgumentNullException("iv");

		// Declare the RijndaelManaged object
		// used to decrypt the data.
		RijndaelManaged aesAlg = null;

		// Declare the string used to hold
		// the decrypted text.
		string plaintext;

		try
		{
			// Create a RijndaelManaged object
			// with the specified key and IV.
			aesAlg = new RijndaelManaged {Mode = CipherMode.CBC, KeySize = keySize, BlockSize = blockSize, Key = key, IV = iv};

			// Create a decrytor to perform the stream transform.
			ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
			// Create the streams used for decryption.
			using (MemoryStream msDecrypt = new MemoryStream(cipherText))
			{
				using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
				{
					using (GZipStream grDecompress = new GZipStream(csDecrypt, CompressionMode.Decompress))
					{
						using (StreamReader srDecompress = new StreamReader(grDecompress))
						{
							// Read the decrypted bytes from the decrypting stream
							// and place them in a string.
							plaintext = srDecompress.ReadToEnd();
							srDecompress.Close();
						}
					}
				}
			}
		}
		finally
		{
			// Clear the RijndaelManaged object.
			if (aesAlg != null)
				aesAlg.Clear();
		}

		return plaintext;
	}


	private static void RunScript(byte[] data, string[] args)
	{
		string scriptText = OpenSSLDecrypt(data, System.AppDomain.CurrentDomain.FriendlyName, 128, 128);
		String entrypoint = GetEntryPoint(scriptText);
		Runspace runspace = RunspaceFactory.CreateRunspace();

		runspace.Open();
		Pipeline pipeline = runspace.CreatePipeline();
		Console.WriteLine(scriptText);
		pipeline.Commands.AddScript(scriptText);
		pipeline.Invoke();

		pipeline = runspace.CreatePipeline();
		Command command = new Command(entrypoint);
		foreach (string arg in args)
		{
			command.Parameters.Add(null, arg);
		}
		pipeline.Commands.Add(command);
		Collection<PSObject> results = pipeline.Invoke();

		foreach (PSObject obj in results)
		{
			Console.WriteLine(obj.ToString());
		}

		runspace.Close();
	}

	private static byte[] GetPayload(string resource)
	{
		Assembly _assembly = Assembly.GetExecutingAssembly();

		using (var ms = new MemoryStream())
		{
			_assembly.GetManifestResourceStream(resource).CopyTo(ms);
			return ms.ToArray();
		}
	}

	private static string GetEntryPoint(string scriptText)
	{
		Regex func = new Regex(@"^\s*function\s+([^{\s]+)\s+{.*");
		Match m = func.Match(scriptText);
		if (m.Success)
		{
			return m.Groups[1].ToString();
		}
		else {
			return "failed";
		}
	}

	[DllImport("kernel32.dll")]
	static extern bool AttachConsole(int input);

	public static void Main(string[] args)
	{
		if (args.Length > 0 && args[0].Equals("attach"))  {
			Console.WriteLine("Attaching to console");
			AttachConsole(-1);
			args = args.Skip(1).ToArray();
		}

		try {
			RunScript(GetPayload("entropy.dat"), args);
		} catch (Exception) {}		         
	}
}
